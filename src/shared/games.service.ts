import { Game } from './game.model';
import { EventEmitter } from '@angular/core';

export class GamesService {
  gamesUpdate = new EventEmitter<Game[]>();
  private games: Game[] = [
    new Game (1,
      'Counter-Strike:Global Offensive',
      'https://s1.gaming-cdn.com/images/products/9459/orig/counter-strike-global-offensive-pc-mac-game-steam-cover.jpg',
      'Steam',
      'Counter-Strike: Global Offensive (CS: GO) expands upon the team-based action gameplay that it pioneered when it was launched 19 years ago. CS: GO features new maps, characters, weapons, and game modes, and delivers updated versions of the classic CS content (de_dust2, etc.).',
      ),
    new Game(2,
      'Dota 2',
      'https://upload.wikimedia.org/wikipedia/ru/8/8e/%D0%9E%D0%B1%D0%BB%D0%BE%D0%B6%D0%BA%D0%B0_Dota_2.jpg',
      'Steam',
      'Every day, millions of players worldwide enter the battle as one of over a hundred Dota Heroes in a 5v5 team clash. Dota is the deepest multi-player action RTS game ever made and there\'s always a new strategy or tactic to discover. It\'s completely free to play and always will be – start defending your ancient now.'),
    new Game(3,
      'FIFA 2022',
      'https://www.digiseller.ru/preview/73392/p1_3202372_95c4ba17.png',
      'Origin',
      'Powered by Football™, EA SPORTS™ FIFA 22 brings the game even closer to the real thing with fundamental gameplay advances and a new season of innovation across every mode.\n' +
      '\n' +
      'EA SPORTS™ FIFA 22 brings the game even closer to the real thing with fundamental gameplay advances and a new season of innovation across every mode. New gameplay features in FIFA 22 give you more consistency between the posts with a goalkeeper…'
      ),
    new Game(4,
      'Ranch Simulator',
      'https://s1.gaming-cdn.com/images/products/8133/orig/game-steam-ranch-simulator-cover.jpg',
      'Epic Games',
      'anch Simulator is a popular game where a player must turn their family’s rundown ranch into one that’s the most prosperous in the valley. To succeed, you must be a builder, a farmer, and a hunter! The Windows single and multiplayer game requires skills and creativity. You can expect a massive simulation as you work on the rundown place you call home. '
      ),
    ]

  getGames() {
    return this.games.slice();
  }
  getGamesByPlatform(platform: string){
    return this.games.filter(game => game.platform === platform);
  }
  getGame(id: number) {
    return this.games.find(game => game.id === id);
  }
  addGame(newGame: Game) {
    const isThere = this.games.find(game => game.name === newGame.name);
    if(!isThere) {
      this.games.push(newGame);
    }
    this.gamesUpdate.emit(this.games);
  }
}
