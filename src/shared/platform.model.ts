export class Platform {
  constructor(
    public name: string,
    public description: string,
    public imgSource: string
  ) {}
}
