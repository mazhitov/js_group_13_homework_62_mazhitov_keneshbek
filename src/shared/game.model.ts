export class Game {
  constructor(
    public id: number,
    public name: string,
    public imageStr: string,
    public platform: string,
    public description: string
  ){}
}
