import { Platform } from './platform.model';
import { EventEmitter } from '@angular/core';

export class PlatformsService {
  platformsChange = new EventEmitter;
  private platforms = [
    new Platform('Steam', 'steam platform', ''),
    new Platform('Origin', 'steam platform', ''),
    new Platform('Epic Games', 'steam platform', ''),
  ];

  getPlatforms() {
    return this.platforms.slice();
  }

  addPlatform(newPlatform: Platform) {
    const isThere = this.platforms.find(platform => platform.name === newPlatform.name);
    if(!isThere) {
      this.platforms.push(newPlatform);
    }
    this.platformsChange.emit(this.platforms);
  }

}
