import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { GamesComponent } from './games/games.component';
import { NewGameComponent } from './new-game/new-game.component';
import { FormsModule } from '@angular/forms';
import { GameComponent } from './games/game/game.component';
import { GamesService } from '../shared/games.service';
import { GameDetailsComponent } from './games/platform-games/game-details/game-details.component';
import { PlatformGamesComponent } from './games/platform-games/platform-games.component';
import { PlatformsItemComponent } from './games/platforms-item/platforms-item.component';
import { PlatformsService } from '../shared/platforms.service';
import { NewPlatformComponent } from './new-platform/new-platform.component';
import { NotFoundComponent } from './not-found.component';
import { EmptyGamesComponent } from './games/platform-games/empty-games.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    GamesComponent,
    NewGameComponent,
    GameComponent,
    GameDetailsComponent,
    PlatformGamesComponent,
    PlatformsItemComponent,
    NewPlatformComponent,
    NotFoundComponent,
    EmptyGamesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [GamesService, PlatformsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
