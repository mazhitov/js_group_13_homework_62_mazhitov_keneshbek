import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NewGameComponent } from './new-game/new-game.component';
import { PlatformGamesComponent } from './games/platform-games/platform-games.component';
import { GameDetailsComponent } from './games/platform-games/game-details/game-details.component';
import { PlatformsItemComponent } from './games/platforms-item/platforms-item.component';
import { NewPlatformComponent } from './new-platform/new-platform.component';
import { NotFoundComponent } from './not-found.component';
import { EmptyGamesComponent } from './games/platform-games/empty-games.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'games/:id', component: GameDetailsComponent},
  {path: 'platforms', component: PlatformsItemComponent, children: [
      {path: '', component: EmptyGamesComponent},
      {path: 'new-game', component: NewGameComponent},
      {path: 'new-platform', component: NewPlatformComponent},
      {path: ':id', component: PlatformGamesComponent},
    ]},
  {path: '**',component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
