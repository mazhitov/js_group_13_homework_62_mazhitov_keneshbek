import { Component, OnInit } from '@angular/core';
import { Game } from '../../../shared/game.model';
import { GamesService } from '../../../shared/games.service';
import { PlatformsService } from '../../../shared/platforms.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Platform } from '../../../shared/platform.model';

@Component({
  selector: 'app-platform-games',
  templateUrl: './platform-games.component.html',
  styleUrls: ['./platform-games.component.css']
})
export class PlatformGamesComponent implements OnInit {
  games: Game[] = [];
  platforms:Platform[] = [];
  platformId!:number;
  constructor(private gamesService: GamesService, private platformService: PlatformsService,
              private route: ActivatedRoute
              ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.platformId = parseInt(params['id']);
      this.platforms = this.platformService.getPlatforms();
      this.games = this.gamesService.getGamesByPlatform(this.platforms[this.platformId]['name']);
    });
  }

}
