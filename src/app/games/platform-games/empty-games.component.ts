import { Component } from '@angular/core';

@Component({
  selector: 'app-empty-games',
  template: `
    <h4>Platform Games list</h4>
    <p>NO platform is selected</p>
  `
})
export class EmptyGamesComponent {}
