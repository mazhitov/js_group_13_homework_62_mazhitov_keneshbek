import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { GamesService } from '../../../../shared/games.service';
import { Game } from '../../../../shared/game.model';

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.component.html',
  styleUrls: ['./game-details.component.css']
})
export class GameDetailsComponent implements OnInit {
  game!:Game;
  constructor(private route: ActivatedRoute, private gamesService: GamesService) { }

  ngOnInit(): void {
    this.route.params.subscribe((params:Params) => {
      const gameId = parseInt(params['id']);
      this.game = this.gamesService.getGame(gameId)!;
    });
  }

}
