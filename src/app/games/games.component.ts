import { Component, OnInit } from '@angular/core';
import { Game } from '../../shared/game.model';
import { GamesService } from '../../shared/games.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})
export class GamesComponent implements OnInit {
  games: Game[] = [];

  constructor(private gamesService: GamesService) { }

  ngOnInit(): void {
    this.games = this.gamesService.getGames();
    this.gamesService.gamesUpdate.subscribe((games:Game[]) => {
      this.games = games;
    })
  }
}
