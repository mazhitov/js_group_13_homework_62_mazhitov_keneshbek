import { Component, OnInit } from '@angular/core';
import { Platform } from '../../../shared/platform.model';
import { PlatformsService } from '../../../shared/platforms.service';

@Component({
  selector: 'app-platforms-item',
  templateUrl: './platforms-item.component.html',
  styleUrls: ['./platforms-item.component.css']
})
export class PlatformsItemComponent implements OnInit {
  platforms: Platform[] = [];
  constructor(private platformService: PlatformsService) { }

  ngOnInit(): void {
    this.platforms = this.platformService.getPlatforms();
    this.platformService.platformsChange.subscribe((platforms:Platform[]) => {
      this.platforms = platforms;
    });
  }

}
