import { Component } from '@angular/core';
import { PlatformsService } from '../../shared/platforms.service';
import { Platform } from '../../shared/platform.model';

@Component({
  selector: 'app-new-platform',
  templateUrl: './new-platform.component.html',
  styleUrls: ['./new-platform.component.css']
})
export class NewPlatformComponent {
  name = '';
  description = '';
  imgSource = '';

  constructor(private platformsService: PlatformsService) {
  }

  getDisabled() {
    return this.name === '' || this.description === '' || this.imgSource === '';
  }

  createPlatform() {
    this.platformsService.addPlatform(new Platform(this.name, this.description, this.imgSource));
    this.reset();
  }

  reset() {
    this.name = '';
    this.description = '';
    this.imgSource = '';
  }
}
