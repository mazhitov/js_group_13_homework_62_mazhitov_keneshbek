import { Component, OnInit } from '@angular/core';
import { GamesService } from '../../shared/games.service';
import { Game } from '../../shared/game.model';
import { Platform } from '../../shared/platform.model';
import { PlatformsService } from '../../shared/platforms.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.css']
})
export class NewGameComponent implements OnInit {
  name = '';
  imageUrl = '';
  platform = 'select platform'
  description = '';
  platforms: Platform[] = [];

  constructor(private gamesService: GamesService, private platformsService: PlatformsService) {
  }

  ngOnInit() {
    this.platforms = this.platformsService.getPlatforms();
  }

  createGame() {
    const games = this.gamesService.getGames();
    let lastGameId = games[games.length - 1]['id'];
    const game = new Game(lastGameId+1, this.name, this.imageUrl, this.platform, this.description);
    this.gamesService.addGame(game);
    this.onReset();
  }

  getDisabled() {
    return this.name === '' || this.imageUrl === '' || this.platform === '' || this.platform === 'select platform';
  }

  onReset() {
    this.name = '';
    this.imageUrl = '';
    this.platform = 'select platform'
    this.description = '';
  }
}
